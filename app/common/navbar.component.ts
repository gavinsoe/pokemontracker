﻿import { Component, EventEmitter,
         Output } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'gs-navbar',
    templateUrl: 'app/common/navbar.component.html',
    styleUrls: ['app/common/navbar.component.css'],
    directives: [ROUTER_DIRECTIVES],
})

export class NavbarComponent {
    @Output() onSidebarToggle = new EventEmitter<boolean>();

    onSidebarToggleClick(): void {
        this.onSidebarToggle.emit(true);
    }
}