System.register(['@angular/core', '../pokemon/pokemon-visible-filter.pipe', '../pokemon/pokemon-name-filter.pipe', '../pokemon/pokemon-info.pipe'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, pokemon_visible_filter_pipe_1, pokemon_name_filter_pipe_1, pokemon_info_pipe_1;
    var SidebarComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (pokemon_visible_filter_pipe_1_1) {
                pokemon_visible_filter_pipe_1 = pokemon_visible_filter_pipe_1_1;
            },
            function (pokemon_name_filter_pipe_1_1) {
                pokemon_name_filter_pipe_1 = pokemon_name_filter_pipe_1_1;
            },
            function (pokemon_info_pipe_1_1) {
                pokemon_info_pipe_1 = pokemon_info_pipe_1_1;
            }],
        execute: function() {
            SidebarComponent = (function () {
                function SidebarComponent(_changeRef) {
                    this._changeRef = _changeRef;
                    this.filterNameChange = new core_1.EventEmitter();
                    this.onFeatureClicked = new core_1.EventEmitter();
                    this.onFilterChange = new core_1.EventEmitter();
                    this.sortByDistance = true;
                }
                SidebarComponent.prototype.onSidebarHideClick = function () {
                    this.animateSidebar();
                };
                SidebarComponent.prototype.animateSidebar = function () {
                    $("#sidebar").animate({
                        width: "toggle"
                    }, 350);
                };
                ;
                SidebarComponent.prototype.ngOnChanges = function () {
                    this.updateList();
                };
                SidebarComponent.prototype.updateList = function () {
                    var _this = this;
                    if (this.markers && this.markers.length) {
                        this.filteredMarkers = new pokemon_visible_filter_pipe_1.PokemonVisibleFilterPipe()
                            .transform(this.markers, this.map);
                        if (this.sortByDistance) {
                            this.filteredMarkers =
                                this.filteredMarkers.sort(function (a, b) {
                                    return new L.LatLng(a.lat, a.lng).distanceTo(_this.currentLocation) -
                                        new L.LatLng(b.lat, b.lng).distanceTo(_this.currentLocation);
                                });
                        }
                        else {
                            this.filteredMarkers =
                                this.filteredMarkers.sort(function (a, b) {
                                    return a.name > b.name ? 1 : ((b.name > a.name) ? -1 : 0);
                                });
                        }
                        this._changeRef.markForCheck();
                    }
                };
                SidebarComponent.prototype.filterChanged = function (newValue) {
                    console.log("change detected : " + newValue);
                    this.filterName = newValue;
                    this.filterNameChange.emit(newValue);
                    this.onFilterChange.emit(newValue);
                };
                SidebarComponent.prototype.featureClicked = function (id) {
                    console.log("Feature clicked : " + id);
                    this.onFeatureClicked.emit(id);
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', L.Map)
                ], SidebarComponent.prototype, "map", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], SidebarComponent.prototype, "markers", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', L.LatLng)
                ], SidebarComponent.prototype, "currentLocation", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], SidebarComponent.prototype, "filterName", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], SidebarComponent.prototype, "filterNameChange", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], SidebarComponent.prototype, "onFeatureClicked", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], SidebarComponent.prototype, "onFilterChange", void 0);
                SidebarComponent = __decorate([
                    core_1.Component({
                        selector: 'gs-sidebar',
                        templateUrl: 'app/common/sidebar.component.html',
                        styleUrls: ['app/common/sidebar.component.css'],
                        changeDetection: core_1.ChangeDetectionStrategy.OnPush,
                        pipes: [pokemon_visible_filter_pipe_1.PokemonVisibleFilterPipe, pokemon_name_filter_pipe_1.PokemonNameFilterPipe, pokemon_info_pipe_1.PokemonInfoPipe]
                    }), 
                    __metadata('design:paramtypes', [core_1.ChangeDetectorRef])
                ], SidebarComponent);
                return SidebarComponent;
            }());
            exports_1("SidebarComponent", SidebarComponent);
        }
    }
});
//# sourceMappingURL=sidebar.component.js.map