System.register(['@angular/core', '@angular/router', 'angular2-google-maps/core', 'rxjs/Observable', '../pokemon/pokemon-marker.service', '../pokemon/pokemon.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, core_2, Observable_1, pokemon_marker_service_1, pokemon_service_1;
    var MapComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (core_2_1) {
                core_2 = core_2_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            },
            function (pokemon_marker_service_1_1) {
                pokemon_marker_service_1 = pokemon_marker_service_1_1;
            },
            function (pokemon_service_1_1) {
                pokemon_service_1 = pokemon_service_1_1;
            }],
        execute: function() {
            MapComponent = (function () {
                function MapComponent(_markerService, _pokemonService) {
                    this._markerService = _markerService;
                    this._pokemonService = _pokemonService;
                    this.title = 'a map?';
                    this.lat = -37.8152065;
                    this.lng = 144.963937;
                    this.fillColor = "#CCCCCC";
                }
                MapComponent.prototype.onClick = function () {
                    this.lat = -34.897;
                    this.lng = 150.844;
                    console.log("lat,lng changed! lat: " + this.lat.toString() + " lng: " + this.lng.toString());
                };
                MapComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    /*this._markerService.getMarkers()
                        .subscribe(markers => this.markers = markers,
                        error => this.errorMessage = <any>error);
                    
                    this._pokemonService.getPokemons()
                        .subscribe(pokemons => this.pokemons = pokemons,
                        error => this.errorMessage = <any>error);*/
                    Observable_1.Observable.forkJoin(this._markerService.getMarkers(), this._pokemonService.getPokemons()).subscribe(function (data) {
                        _this.markers = data[0];
                        _this.pokemons = data[1];
                    }, function (error) { return _this.errorMessage = error; });
                };
                MapComponent = __decorate([
                    core_1.Component({
                        selector: 'gs-map',
                        templateUrl: 'app/map/map.component.html',
                        styleUrls: ['app/map/map.component.css'],
                        directives: [router_1.ROUTER_DIRECTIVES, core_2.GOOGLE_MAPS_DIRECTIVES],
                        providers: [pokemon_marker_service_1.PokemonMarkerService, pokemon_service_1.PokemonService]
                    }), 
                    __metadata('design:paramtypes', [pokemon_marker_service_1.PokemonMarkerService, pokemon_service_1.PokemonService])
                ], MapComponent);
                return MapComponent;
            }());
            exports_1("MapComponent", MapComponent);
        }
    }
});
//# sourceMappingURL=map.component.js.map