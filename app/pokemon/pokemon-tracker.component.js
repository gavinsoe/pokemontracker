System.register(['@angular/core', '@angular/router', 'angular2-google-maps/core', 'rxjs/Observable', './pokemon-marker.service', './pokemon.service', '../common/geolocation.service', '../common/map.component', '../common/navbar.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, core_2, Observable_1, pokemon_marker_service_1, pokemon_service_1, geolocation_service_1, map_component_1, navbar_component_1;
    var PokemonTrackerComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (core_2_1) {
                core_2 = core_2_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            },
            function (pokemon_marker_service_1_1) {
                pokemon_marker_service_1 = pokemon_marker_service_1_1;
            },
            function (pokemon_service_1_1) {
                pokemon_service_1 = pokemon_service_1_1;
            },
            function (geolocation_service_1_1) {
                geolocation_service_1 = geolocation_service_1_1;
            },
            function (map_component_1_1) {
                map_component_1 = map_component_1_1;
            },
            function (navbar_component_1_1) {
                navbar_component_1 = navbar_component_1_1;
            }],
        execute: function() {
            PokemonTrackerComponent = (function () {
                function PokemonTrackerComponent(_markerService, _pokemonService, _geolocationService, _changeRef) {
                    this._markerService = _markerService;
                    this._pokemonService = _pokemonService;
                    this._geolocationService = _geolocationService;
                    this._changeRef = _changeRef;
                }
                PokemonTrackerComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    Observable_1.Observable.forkJoin(this._markerService.getMarkers(), this._pokemonService.getPokemons()).subscribe(function (data) {
                        _this.markers = data[0];
                        _this.markers.map(function (m) {
                            m.name = data[1][m.pkmn_id].name,
                                m.iconUrl = data[1][m.pkmn_id].iconUrl;
                        });
                        _this._changeRef.markForCheck();
                    }, function (error) { return _this.errorMessage = error; });
                };
                // Toggle sidebar
                PokemonTrackerComponent.prototype.toggleSidebar = function () {
                    this._mapComponent.toggleSidebar();
                };
                __decorate([
                    core_1.ViewChild(map_component_1.MapComponent), 
                    __metadata('design:type', map_component_1.MapComponent)
                ], PokemonTrackerComponent.prototype, "_mapComponent", void 0);
                PokemonTrackerComponent = __decorate([
                    core_1.Component({
                        selector: 'pkmn-tracker',
                        templateUrl: 'app/pokemon/pokemon-tracker.component.html',
                        styleUrls: ['app/pokemon/pokemon-tracker.component.css'],
                        changeDetection: core_1.ChangeDetectionStrategy.OnPush,
                        directives: [router_1.ROUTER_DIRECTIVES,
                            core_2.GOOGLE_MAPS_DIRECTIVES,
                            map_component_1.MapComponent,
                            navbar_component_1.NavbarComponent],
                        providers: [pokemon_marker_service_1.PokemonMarkerService,
                            pokemon_service_1.PokemonService,
                            geolocation_service_1.GeolocationService]
                    }), 
                    __metadata('design:paramtypes', [pokemon_marker_service_1.PokemonMarkerService, pokemon_service_1.PokemonService, geolocation_service_1.GeolocationService, core_1.ChangeDetectorRef])
                ], PokemonTrackerComponent);
                return PokemonTrackerComponent;
            }());
            exports_1("PokemonTrackerComponent", PokemonTrackerComponent);
        }
    }
});
//# sourceMappingURL=pokemon-tracker.component.js.map