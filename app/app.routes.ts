﻿import { provideRouter, RouterConfig } from '@angular/router';

import { PokemonTrackerComponent } from './pokemon/pokemon-tracker.component';

const routes: RouterConfig = [
    { path: '', component: PokemonTrackerComponent }
];

export const appRouterProviders = [
    provideRouter(routes)
];