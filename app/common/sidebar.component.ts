﻿import { Component,
         Input, Output, EventEmitter,
         OnInit, OnChanges,
         ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';

import { IPokemonMarker } from '../pokemon/pokemon-marker';

import { PokemonVisibleFilterPipe } from '../pokemon/pokemon-visible-filter.pipe';
import { PokemonNameFilterPipe } from '../pokemon/pokemon-name-filter.pipe';
import { PokemonInfoPipe} from '../pokemon/pokemon-info.pipe';

@Component({
    selector: 'gs-sidebar',
    templateUrl: 'app/common/sidebar.component.html',
    styleUrls: ['app/common/sidebar.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    pipes: [PokemonVisibleFilterPipe, PokemonNameFilterPipe, PokemonInfoPipe]
})

export class SidebarComponent
    implements OnChanges{

    @Input() map: L.Map;
    @Input() markers: IPokemonMarker[];
    @Input() currentLocation: L.LatLng;
    @Input() filterName: string;
    @Output() filterNameChange = new EventEmitter();
    @Output() onFeatureClicked = new EventEmitter();
    @Output() onFilterChange = new EventEmitter();

    sortByDistance: boolean = true;

    filteredMarkers: IPokemonMarker[];

    constructor(private _changeRef: ChangeDetectorRef) { }
    
    onSidebarHideClick(): void {
        this.animateSidebar();
    }

    animateSidebar(): void {
        $("#sidebar").animate({
            width: "toggle"
        }, 350)
    };        

    ngOnChanges() {
        this.updateList();
    }

    updateList() {
        if (this.markers && this.markers.length) {
            this.filteredMarkers = new PokemonVisibleFilterPipe()
                .transform(this.markers, this.map);

            if (this.sortByDistance) {
                this.filteredMarkers =
                    this.filteredMarkers.sort((a, b) =>
                        new L.LatLng(a.lat, a.lng).distanceTo(this.currentLocation) -
                        new L.LatLng(b.lat, b.lng).distanceTo(this.currentLocation));
            } else {
                this.filteredMarkers =
                    this.filteredMarkers.sort((a, b) =>
                        a.name > b.name ? 1 : ((b.name > a.name) ? -1 : 0));
            }

            this._changeRef.markForCheck();
        }
    }

    filterChanged(newValue: string) {
        console.log("change detected : " + newValue);
        this.filterName = newValue;
        this.filterNameChange.emit(newValue);
        this.onFilterChange.emit(newValue);
    }

    featureClicked(id: number) {
        console.log("Feature clicked : " + id);
        this.onFeatureClicked.emit(id);
    }
    
}