﻿import { Injectable } from '@angular/core';
import { IPokemonMarker } from './pokemon-marker';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PokemonMarkerService {
    private _pkmnMarkerSvc = 'api/pokemon/pokemon-markers.json';

    constructor(private _http: Http) { }
    
    getMarkers(): Observable<IPokemonMarker[]> {
        return this._http.get(this._pkmnMarkerSvc)
            .map((response: Response) => <IPokemonMarker[]>response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}