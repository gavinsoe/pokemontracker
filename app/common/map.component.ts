﻿import { Component, Input,
         OnInit, OnChanges,
         ViewChild,
         ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';

import { IPokemonMarker } from '../pokemon/pokemon-marker';
import { SidebarComponent } from '../common/sidebar.component';

import { PokemonNameFilterPipe } from '../pokemon/pokemon-name-filter.pipe';

@Component({
    selector: 'gs-map',
    templateUrl: 'app/common/map.component.html',
    styleUrls: ['app/common/map.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    directives: [SidebarComponent],
    pipes: [PokemonNameFilterPipe]
})


export class MapComponent
    implements OnInit, OnChanges {

    @Input() markers: IPokemonMarker[];    

    @ViewChild(SidebarComponent)
    private _sidebarComponent: SidebarComponent;

    currentLocation: L.LatLng = new L.LatLng(-37.8142175, 144.9631608);
    map: L.Map;
    filterName: string = '';
    markerCluster: L.MarkerClusterGroup
    locationMarker: L.Marker;

    constructor(private _changeRef: ChangeDetectorRef) { }

    ngOnInit(): void {
        // Marker cluster
        this.markerCluster = new L.MarkerClusterGroup({
            spiderfyOnMaxZoom: true,
            showCoverageOnHover: false,
            zoomToBoundsOnClick: true,
            disableClusteringAtZoom: 16
        });

        // Location marker
        this.locationMarker = new L.Marker(this.currentLocation);

        // Init the map
        this.map = new L.Map('map', {
            center: new L.LatLng(this.currentLocation.lat, this.currentLocation.lng),
            zoom: 15,
            maxZoom: 18,
            layers: [this.markerCluster, this.locationMarker]
        });
        
        // Basemap layer
        L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ2F2aW5zb2UiLCJhIjoiY2lxeDhrdG5sMDFmdmZwbWc4dGhsdTBmZyJ9.qDeBnKAjSXkn12jVAzY4YQ', {
            attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            maxZoom: 18,
            id: 'your.mapbox.project.id',
            accessToken: 'your.mapbox.public.access.token',
            layers: [this.markerCluster]
        }).addTo(this.map);

        // event binding        
        this.map.on("moveend", (e) =>
            this._sidebarComponent.updateList());

        // Location services
        L.control.locate({
            drawCircle: false,
            drawMarker: false,
            keepCurrentZoomLevel: true
        }).addTo(this.map);
        
        // when location is found...
        this.map.on('locationfound', ((e: L.LeafletLocationEvent) => {
            console.log("location found : (" + e.latlng.lat + "," + e.latlng.lng + ")");
            this.updateLocationMarker(e.latlng.lat, e.latlng.lng);
        }));

        // Geocoder
        L.Control.geocoder({ defaultMarkGeocode: false })
            .on('markgeocode', ((e) => {
                console.log("location found : (" + e.geocode.properties.lat + "," + e.geocode.properties.lon + ")");
                this.updateLocationMarker(e.geocode.properties.lat, e.geocode.properties.lon);       
                this.updateView(this.currentLocation);         
            })).addTo(this.map);
    }

    ngOnChanges(): void {
        this.updateMarkers();
    }
    
    updateMarkers(): void {
        // Add markers into the cluster        
        if (this.markers && this.markers.length) {
            var mapMarkers : L.Marker[] = []

            new PokemonNameFilterPipe()
                .transform(this.markers, this.filterName)
                .forEach(m => {
                mapMarkers.push(new L.Marker([m.lat, m.lng],
                    {
                        title: m.name,
                        icon: new L.Icon({
                            iconUrl: m.iconUrl,
                            iconSize: [60, 60]
                        })
                    }
                ))
            });
            console.log("Resetting markers | filter : " + this.filterName);
            this.markerCluster.clearLayers();
            this.markerCluster.addLayers(mapMarkers);
        }
        else {
            console.log("markers is empty");
        }
    }

    updateLocationMarker(lat: number, lng: number): void {
        this.currentLocation.lat = lat;
        this.currentLocation.lng = lng;
        this.locationMarker.setLatLng(this.currentLocation);
    }

    updateView(location: L.LatLng): void {
        this.map.setView(location, this.map.getZoom(), { animate: true });
        this.map.stopLocate();
    }

    focusMarker(id: number) {
        console.log("Search for marker with id : " + id);
        var selectedMarker = this.markers.find(m => m.id == id);
        console.log("Selected marker: (" + selectedMarker.lat + "," + selectedMarker.lng + ")");
        this.updateView(new L.LatLng(selectedMarker.lat, selectedMarker.lng));
    }

    toggleSidebar(): void {
        this._sidebarComponent.animateSidebar();
        this.map.invalidateSize(true);
    }
}