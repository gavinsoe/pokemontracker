import { bootstrap } from '@angular/platform-browser-dynamic';

// Our main component
import { AppComponent } from './app.component';
import { appRouterProviders } from './app.routes';
import { GOOGLE_MAPS_PROVIDERS, provideLazyMapsAPILoaderConfig} from 'angular2-google-maps/core';

bootstrap(AppComponent, [
    appRouterProviders,
    GOOGLE_MAPS_PROVIDERS,
    provideLazyMapsAPILoaderConfig({
        apiKey: 'AIzaSyCA5LNGShONrLok_isCtVN3Y-SIzIU32nM'
    })
]).catch(err => console.error(err));