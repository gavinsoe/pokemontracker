﻿export interface IPokemonMarker {
    id: number;
    lat: number;
    lng: number;
    pkmn_id: number;
    name: string;
    iconUrl: string;
}