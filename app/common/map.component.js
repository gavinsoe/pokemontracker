System.register(['@angular/core', '../common/sidebar.component', '../pokemon/pokemon-name-filter.pipe'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, sidebar_component_1, pokemon_name_filter_pipe_1;
    var MapComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (sidebar_component_1_1) {
                sidebar_component_1 = sidebar_component_1_1;
            },
            function (pokemon_name_filter_pipe_1_1) {
                pokemon_name_filter_pipe_1 = pokemon_name_filter_pipe_1_1;
            }],
        execute: function() {
            MapComponent = (function () {
                function MapComponent(_changeRef) {
                    this._changeRef = _changeRef;
                    this.currentLocation = new L.LatLng(-37.8142175, 144.9631608);
                    this.filterName = '';
                }
                MapComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    // Marker cluster
                    this.markerCluster = new L.MarkerClusterGroup({
                        spiderfyOnMaxZoom: true,
                        showCoverageOnHover: false,
                        zoomToBoundsOnClick: true,
                        disableClusteringAtZoom: 16
                    });
                    // Location marker
                    this.locationMarker = new L.Marker(this.currentLocation);
                    // Init the map
                    this.map = new L.Map('map', {
                        center: new L.LatLng(this.currentLocation.lat, this.currentLocation.lng),
                        zoom: 15,
                        maxZoom: 18,
                        layers: [this.markerCluster, this.locationMarker]
                    });
                    // Basemap layer
                    L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ2F2aW5zb2UiLCJhIjoiY2lxeDhrdG5sMDFmdmZwbWc4dGhsdTBmZyJ9.qDeBnKAjSXkn12jVAzY4YQ', {
                        attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                        maxZoom: 18,
                        id: 'your.mapbox.project.id',
                        accessToken: 'your.mapbox.public.access.token',
                        layers: [this.markerCluster]
                    }).addTo(this.map);
                    // event binding        
                    this.map.on("moveend", function (e) {
                        return _this._sidebarComponent.updateList();
                    });
                    // Location services
                    L.control.locate({
                        drawCircle: false,
                        drawMarker: false,
                        keepCurrentZoomLevel: true
                    }).addTo(this.map);
                    // when location is found...
                    this.map.on('locationfound', (function (e) {
                        console.log("location found : (" + e.latlng.lat + "," + e.latlng.lng + ")");
                        _this.updateLocationMarker(e.latlng.lat, e.latlng.lng);
                    }));
                    // Geocoder
                    L.Control.geocoder({ defaultMarkGeocode: false })
                        .on('markgeocode', (function (e) {
                        console.log("location found : (" + e.geocode.properties.lat + "," + e.geocode.properties.lon + ")");
                        _this.updateLocationMarker(e.geocode.properties.lat, e.geocode.properties.lon);
                        _this.updateView(_this.currentLocation);
                    })).addTo(this.map);
                };
                MapComponent.prototype.ngOnChanges = function () {
                    this.updateMarkers();
                };
                MapComponent.prototype.updateMarkers = function () {
                    // Add markers into the cluster        
                    if (this.markers && this.markers.length) {
                        var mapMarkers = [];
                        new pokemon_name_filter_pipe_1.PokemonNameFilterPipe()
                            .transform(this.markers, this.filterName)
                            .forEach(function (m) {
                            mapMarkers.push(new L.Marker([m.lat, m.lng], {
                                title: m.name,
                                icon: new L.Icon({
                                    iconUrl: m.iconUrl,
                                    iconSize: [60, 60]
                                })
                            }));
                        });
                        console.log("Resetting markers | filter : " + this.filterName);
                        this.markerCluster.clearLayers();
                        this.markerCluster.addLayers(mapMarkers);
                    }
                    else {
                        console.log("markers is empty");
                    }
                };
                MapComponent.prototype.updateLocationMarker = function (lat, lng) {
                    this.currentLocation.lat = lat;
                    this.currentLocation.lng = lng;
                    this.locationMarker.setLatLng(this.currentLocation);
                };
                MapComponent.prototype.updateView = function (location) {
                    this.map.setView(location, this.map.getZoom(), { animate: true });
                    this.map.stopLocate();
                };
                MapComponent.prototype.focusMarker = function (id) {
                    console.log("Search for marker with id : " + id);
                    var selectedMarker = this.markers.find(function (m) { return m.id == id; });
                    console.log("Selected marker: (" + selectedMarker.lat + "," + selectedMarker.lng + ")");
                    this.updateView(new L.LatLng(selectedMarker.lat, selectedMarker.lng));
                };
                MapComponent.prototype.toggleSidebar = function () {
                    this._sidebarComponent.animateSidebar();
                    this.map.invalidateSize(true);
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], MapComponent.prototype, "markers", void 0);
                __decorate([
                    core_1.ViewChild(sidebar_component_1.SidebarComponent), 
                    __metadata('design:type', sidebar_component_1.SidebarComponent)
                ], MapComponent.prototype, "_sidebarComponent", void 0);
                MapComponent = __decorate([
                    core_1.Component({
                        selector: 'gs-map',
                        templateUrl: 'app/common/map.component.html',
                        styleUrls: ['app/common/map.component.css'],
                        changeDetection: core_1.ChangeDetectionStrategy.OnPush,
                        directives: [sidebar_component_1.SidebarComponent],
                        pipes: [pokemon_name_filter_pipe_1.PokemonNameFilterPipe]
                    }), 
                    __metadata('design:paramtypes', [core_1.ChangeDetectorRef])
                ], MapComponent);
                return MapComponent;
            }());
            exports_1("MapComponent", MapComponent);
        }
    }
});
//# sourceMappingURL=map.component.js.map