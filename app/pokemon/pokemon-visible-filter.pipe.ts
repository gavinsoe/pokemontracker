﻿import { PipeTransform, Pipe } from '@angular/core';
import { IPokemonMarker } from './pokemon-marker';

@Pipe({
    name: 'pokemonVisibleFilter'
})

export class PokemonVisibleFilterPipe implements PipeTransform {
    transform(value: IPokemonMarker[], args: L.Map): IPokemonMarker[] {
        console.log("filtering visible markers");
        return value.filter(m => 
            args.getBounds().contains(new L.LatLng(m.lat, m.lng)));
    }
}