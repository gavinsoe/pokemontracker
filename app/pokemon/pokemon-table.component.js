System.register(['@angular/core', './pokemon-range-filter.pipe'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, pokemon_range_filter_pipe_1;
    var PokemonTableComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (pokemon_range_filter_pipe_1_1) {
                pokemon_range_filter_pipe_1 = pokemon_range_filter_pipe_1_1;
            }],
        execute: function() {
            PokemonTableComponent = (function () {
                function PokemonTableComponent() {
                }
                PokemonTableComponent.prototype.ngOnChanges = function () { };
                PokemonTableComponent.prototype.ngOnInit = function () {
                    console.log("OnInit table | range : " + this.rangeFilter.toString());
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], PokemonTableComponent.prototype, "markers", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Array)
                ], PokemonTableComponent.prototype, "pokemons", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Number)
                ], PokemonTableComponent.prototype, "rangeFilter", void 0);
                PokemonTableComponent = __decorate([
                    core_1.Component({
                        selector: 'pkmn-table',
                        templateUrl: 'app/pokemon/pokemon-table.component.html',
                        pipes: [pokemon_range_filter_pipe_1.PokemonRangeFilterPipe],
                    }), 
                    __metadata('design:paramtypes', [])
                ], PokemonTableComponent);
                return PokemonTableComponent;
            }());
            exports_1("PokemonTableComponent", PokemonTableComponent);
        }
    }
});
//# sourceMappingURL=pokemon-table.component.js.map