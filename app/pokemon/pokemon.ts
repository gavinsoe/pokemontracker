﻿export interface IPokemon {
    id: number;
    name: string;
    iconUrl: string;
}
