﻿import { Injectable } from '@angular/core';
import { IPokemon } from './pokemon';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PokemonService {
    private _pokemonSvc = 'api/pokemon/pokemons.json';

    constructor(private _http: Http) { }
    
    getPokemons(): Observable<IPokemon[]> {
        return this._http.get(this._pokemonSvc)
            .map((response: Response) => <IPokemon[]>response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}