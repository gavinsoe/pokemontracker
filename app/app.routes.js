System.register(['@angular/router', './pokemon/pokemon-tracker.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, pokemon_tracker_component_1;
    var routes, appRouterProviders;
    return {
        setters:[
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (pokemon_tracker_component_1_1) {
                pokemon_tracker_component_1 = pokemon_tracker_component_1_1;
            }],
        execute: function() {
            routes = [
                { path: '', component: pokemon_tracker_component_1.PokemonTrackerComponent }
            ];
            exports_1("appRouterProviders", appRouterProviders = [
                router_1.provideRouter(routes)
            ]);
        }
    }
});
//# sourceMappingURL=app.routes.js.map