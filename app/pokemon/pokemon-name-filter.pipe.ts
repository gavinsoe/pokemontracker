﻿import { PipeTransform, Pipe } from '@angular/core';
import { IPokemonMarker } from './pokemon-marker';

@Pipe({
    name: 'pokemonNameFilter'
})

export class PokemonNameFilterPipe implements PipeTransform {
    transform(value: IPokemonMarker[], filter: string): IPokemonMarker[] {
        filter = filter ? filter.toLocaleLowerCase() : null;
        console.log("Filtering name : " + filter);
        return filter ? value.filter((marker: IPokemonMarker) =>
            marker.name.toLocaleLowerCase().indexOf(filter) !== -1) : value;
    }
}