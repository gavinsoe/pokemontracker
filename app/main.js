System.register(['@angular/platform-browser-dynamic', './app.component', './app.routes', 'angular2-google-maps/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var platform_browser_dynamic_1, app_component_1, app_routes_1, core_1;
    return {
        setters:[
            function (platform_browser_dynamic_1_1) {
                platform_browser_dynamic_1 = platform_browser_dynamic_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (app_routes_1_1) {
                app_routes_1 = app_routes_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [
                app_routes_1.appRouterProviders,
                core_1.GOOGLE_MAPS_PROVIDERS,
                core_1.provideLazyMapsAPILoaderConfig({
                    apiKey: 'AIzaSyCA5LNGShONrLok_isCtVN3Y-SIzIU32nM'
                })
            ]).catch(function (err) { return console.error(err); });
        }
    }
});
//# sourceMappingURL=main.js.map