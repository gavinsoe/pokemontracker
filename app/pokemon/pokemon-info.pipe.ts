﻿import { PipeTransform, Pipe } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { IPokemonMarker } from './pokemon-marker';

@Pipe({
    name: 'pokemonInfo'
})

export class PokemonInfoPipe implements PipeTransform {
    transform(marker: IPokemonMarker, arg: L.LatLng): string {
        var numberPipe = new DecimalPipe();
        if (arg) {            
            var distance: number = arg.distanceTo(new L.LatLng(marker.lat, marker.lng));
            if (distance > 1000) {
                return marker.name + " ~ " + numberPipe.transform((distance / 1000),"1.0-0") + "km";
            } else {
                return marker.name + " ~ " + numberPipe.transform(distance, "1.0-0") + "m";
            }
        }
        else {
            return marker.name;
        }
        
    }
}