﻿import { Component, OnInit,
         ViewChild,
         ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { GOOGLE_MAPS_DIRECTIVES } from 'angular2-google-maps/core';
import { Observable } from 'rxjs/Observable';

import { IPokemon } from './pokemon';
import { IPokemonMarker } from './pokemon-marker';
import { PokemonMarkerService } from './pokemon-marker.service';
import { PokemonService } from './pokemon.service';
import { GeolocationService } from '../common/geolocation.service';

import { MapComponent } from '../common/map.component';
import { NavbarComponent } from '../common/navbar.component';

@Component({
    selector: 'pkmn-tracker',
    templateUrl: 'app/pokemon/pokemon-tracker.component.html',
    styleUrls: ['app/pokemon/pokemon-tracker.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    directives: [ROUTER_DIRECTIVES,
        GOOGLE_MAPS_DIRECTIVES,
        MapComponent,
        NavbarComponent],
    providers: [PokemonMarkerService,
        PokemonService,
        GeolocationService]
})


export class PokemonTrackerComponent
    implements OnInit {

    @ViewChild(MapComponent)
    private _mapComponent: MapComponent;
    
    errorMessage: string;

    markers: IPokemonMarker[];

    constructor(private _markerService: PokemonMarkerService,
        private _pokemonService: PokemonService,
        private _geolocationService: GeolocationService,
        private _changeRef: ChangeDetectorRef) { }

    ngOnInit(): void {
        Observable.forkJoin(
            this._markerService.getMarkers(),
            this._pokemonService.getPokemons()
        ).subscribe(
            data => {
                this.markers = data[0];
                this.markers.map(m => {
                    m.name = data[1][m.pkmn_id].name,
                    m.iconUrl = data[1][m.pkmn_id].iconUrl
                });
                this._changeRef.markForCheck();
            },
            error => this.errorMessage = <any>error
        );
    }

    // Toggle sidebar
    toggleSidebar(): void {
        this._mapComponent.toggleSidebar();
    }
}